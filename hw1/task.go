package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	savedData []byte
)

func getFun(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Stored data was requested.")
	fmt.Println(savedData)
	w.Write(savedData)
}

func replaceFun(w http.ResponseWriter, r *http.Request) {
	newData, _ := ioutil.ReadAll(r.Body)
	fmt.Println("Recieved data:")
	fmt.Println(newData)

	savedData = newData
	w.WriteHeader(http.StatusOK)
}

func main() {
	http.HandleFunc("/get", getFun)
	http.HandleFunc("/replace", replaceFun)

	fmt.Println("Server started!")
	http.ListenAndServe(":8080", nil)
}
