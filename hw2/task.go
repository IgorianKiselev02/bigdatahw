package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

type record struct {
	time time.Time
	data []byte
}

var (
	snapshot  []byte      = []byte{}
	records   []record    = []record{}
	reqChanel chan []byte = make(chan []byte)
	rLock     sync.RWMutex
)

func getFun(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Stored data was requested.")
	if len(records) == 0 {
		w.Write([]byte{})
	} else {
		w.Write(records[len(records)-1].data)
	}
}

func replaceFun(w http.ResponseWriter, r *http.Request) {
	newData, _ := ioutil.ReadAll(r.Body)

	fmt.Println("Recieved data:")
	fmt.Println(newData)
	reqChanel <- newData

	w.WriteHeader(http.StatusOK)
}

func main() {
	go func() {
		minuteTicker := time.NewTicker(time.Minute)

		for {
			select {
			case <-minuteTicker.C:
				rLock.RLock()
				snapshot = append([]byte{}, records[len(records)-1].data...)
				rLock.RUnlock()
			case data := <-reqChanel:
				rLock.Lock()
				records = append(records, record{time: time.Now(), data: data})
				rLock.Unlock()
			}
		}
	}()

	http.HandleFunc("/get", getFun)
	http.HandleFunc("/replace", replaceFun)

	fmt.Println("Server started!")
	http.ListenAndServe(":8080", nil)
}
